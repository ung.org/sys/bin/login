/*
 * UNG's Not GNU
 *
 * Copyright (c) 2020 Jakob Kaivo <jkk@ung.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _XOPEN_SOURCE 700
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#ifndef PATH_MAX
#define PATH_MAX _POSIX_PATH_MAX
#endif

#ifndef PASS_MAX
#define PASS_MAX 1024
#endif

#define PASSWD_FILE	"/etc/passwd"

#ifndef LOGIN_NAME_MAX
#define LOGIN_NAME_MAX 128
#endif

/* marked obsolete in Issue 5, removed in Issue 6, but still present in libc */
char *getpass(const char*);

static int login(const char *username, char * volatile password)
{
	char path[] = "/bin";
	char user[LOGIN_NAME_MAX] = "";
	char pass[PASS_MAX] = "";
	intmax_t uid = 0;
	intmax_t gid = 0;
	char home[PATH_MAX] = "";
	char shell[PATH_MAX] = "";

	FILE *pfile = fopen(PASSWD_FILE, "r");
	if (pfile == NULL) {
		perror("login: password database");
		return 1;
	}

	while (fscanf(pfile, "%[^:]:%[^:]:%jd:%jd:%*[^:]:%[^:]:%[^:\n]\n",
		user, pass, &uid, &gid, home, shell) == 6) {
		if (strcmp(username, user) == 0) {
			break;
		}
	}

	fclose(pfile);

	char *cpass = crypt(password, pass);

	if (password[0] != '\0') {
		memset(password, '\0', strlen(password));
		if (strlen(pass) > 0) {
			return 1;
		}
	}

	if (strlen(pass) > 0 && cpass != NULL && strcmp(cpass, pass) != 0) {
		return 1;
	}

	if (strcmp(username, user) != 0) {
		return 1;
	}
	
	uid_t saved_uid = getuid();
	if (setuid((uid_t)uid) != 0) {
		perror("login: setting uid");
		return 1;
	}

	/* TODO: groups */

	if (chdir(home) != 0) {
		fprintf(stderr, "login: %s: %s\n", home, strerror(errno));
		fprintf(stderr, "login: setting HOME=/\n");
		strcpy(home, "/");
	}

	/* COLUMNS: inherited */
	setenv("DATEMSK", "/etc/datemsk", 1);
	setenv("ENV", "${HOME}/.profile", 1);
	setenv("HOME", home, 1);
	/* LINES: inherited */
	/* do *not* set MSGVERB by default */
	setenv("LOGNAME", username, 1);
	setenv("PATH", path, 1);
	setenv("PWD", home, 1);
	setenv("SHELL", shell, 1);
	setenv("TMPDIR", "/tmp", 1);
	/* TERM: inherited */
	/* TODO: TZ */

	/* TODO: pututmpx() */
	
	execl(shell, shell, (char*)NULL);
	fprintf(stderr, "login: /bin/sh: %s\n", strerror(errno));

	setuid(saved_uid);
	return 0;
}

int main(int argc, char *argv[])
{
	char username[LOGIN_NAME_MAX + 1] = "";
	char *password = NULL;

	int c;
	while ((c = getopt(argc, argv, "")) != -1) {
		switch (c) {
		default:
			return 1;
		}
	}

	if (argc > optind) {
		fprintf(stderr, "login: unexpected operand\n");
		return 1;
	}

	for (;;) {
		chdir("/");

		printf("login: ");
		fflush(stdout);
		if (fgets(username, sizeof(username), stdin) == NULL) {
			return 0;
		}
		username[strlen(username) - 1] = '\0';

		password = getpass("password: ");
		if (password == NULL) {
			return 0;
		}

		if (login(username, password) != 0) {
			printf("incorrect username or password\n\n");
		}
	}
}
